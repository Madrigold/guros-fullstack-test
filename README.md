# Guros Fullstack Test
Para poder correr en local la aplicación lo primero es instalar Python 3
Posterior a esto se requiere instalar pip
Una vez instalado pip es necesario instalar flask
Y por último es correr el comando 'python3 flask_RESTful_ADNMutation.py' para iniciar los servicios.  
Ya que se ha instalado todo, podemos comenzar a correr el proyecto.

Para que podamos guardar la información, se requiere crear una base de datos y una tabla así:

CREATE TABLE ADN (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  sequence TEXT NOT NULL,
  hasMutation TEXT NOT NULL
);
(cabe mencionar que la BD que se creó en el ambiente de prubeas fue SQLite)

De esta manera podremos guardar la información básica y realizar las consultas necesarias.

La dirección IP de la api es 192.241.144.20 y viaja por el puerto 5000

Para la consulta GET, usaremos http://192.241.144.20:5000/stats

Para la consulta POST, usaremos http://192.241.144.20:5000/mutation

Test Desdigne for Guros 
