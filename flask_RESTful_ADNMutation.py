import flask
from flask import request, jsonify
import sqlite3
from sqlite3 import Error


app = flask.Flask(__name__)
app.config["DEBUG"] = True


@app.route ('/', methods=['POST'])
def home():
    return 'Web Service'


# endpoint POST /mutation
@app.route('/mutation', methods=['POST'])
def hasMutation():
    jsonify(request.json)
    dna = request.json
    coincidence = 0
    rows = 0
    data = dna["dna"]
    n = len(data)
    database = r"/home/guros-fullstack-test/guros.db"
    conn = create_connection(database)
    result = False

    for row in data:
        l = len(row)
        columns = 0

        for column in row:

            if rows < n - 3:

                if columns < l - 3:
                    d = ""
                    g = data[rows][columns]
                    d += g
                    d += data[rows + 1][columns + 1]
                    d += data[rows + 2][columns + 2]
                    d += data[rows + 3][columns + 3]
                    same_g = d.replace(g, "")
                    if len(same_g) == 0:
                        coincidence += 1

                v = ""
                g = data[rows][columns]
                v += g
                v += data[rows + 1][columns]
                v += data[rows + 2][columns]
                v += data[rows + 3][columns]
                same_g = v.replace(g, "")
                if len(same_g) == 0:
                    coincidence += 1

            if (columns < l - 3):
                h = ""
                g = data[rows][columns]
                h += g
                h += data[rows][columns + 1]
                h += data[rows][columns + 2]
                h += data[rows][columns + 3]
                same_g = h.replace(g, "")
                if len(same_g) == 0:
                    coincidence += 1
            columns += 1
        rows += 1
    print(coincidence)
    if coincidence > 1:
        result = True
        insert_dna(conn, data, result)
        return jsonify({'Success': 'Mutation Found'}), 200

    else:
        insert_dna(conn, data, result)
        return jsonify({'Error': 'Mutation not found'}), 403



# endpoint GET /stats
@app.route('/stats', methods=['GET'])
def get_stats():
    database = r"/home/guros-fullstack-test/guros.db"
    conn = create_connection(database)
    mutations = get_info(conn, 1)
    no_mutations = get_info(conn, 2)
    ratio = float(mutations) / float(no_mutations)

    response = {
        'count_mutations': mutations,
        'count_no_mutation': no_mutations,
        'ratio': ratio
    }
    return jsonify(response)

def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by the db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)
    return conn

def insert_dna(conn, dna, value):
    """
    :param conn: the Connection object
    :return:
    """
    dna = str(dna)
    value = str(value)
    cur = conn.cursor()
    cur.execute("insert into ADN ('sequence', 'hasMutation') values (?, ?)", (dna, value))
    conn.commit()

def get_info(conn, id):
    cur = conn.cursor()
    if id == 1:
        cur.execute("select count(hasMutation) from ADN  where hasMutation = 'True'")
    else:
        cur.execute("select count(hasMutation) from ADN  where hasMutation = 'False'")

    rows = cur.fetchone()[0]
    return rows

app.run(host='192.241.144.20', port='5000')
